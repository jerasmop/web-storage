function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  localStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  localStorage.setItem("json", JSON.stringify(objeto));
  contrarItemEnWebStorage();
  listarItems();
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "VALOR OBTENIDO: " + valor;
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);

}

function elimnarEnWebStorage() {
  var txtClave = document.getElementById("txtClave").value; /* Referencia al input de clave */
  localStorage.removeItem(txtClave);
  console.log(localStorage);
  contrarItemEnWebStorage();
  listarItems();
 }

function limpiarEnWebStorage() {
  localStorage.clear();
  contrarItemEnWebStorage();
  listarItems();
}

function contrarItemEnWebStorage() {
  var valor = "CANTIDAD DE ITEMS EN STORAGE: " + localStorage.length;
  var spanValor = document.getElementById("spanContar");
   spanValor.innerText = valor;
}

function listarItems(){
  var tabla  = document.getElementById("tableItem");
  var tableString = '<table class=\"table"><thead><tr><td>LLAVE</td><td>VALOR</td></thead></tr>';
  for (var i = 0; i < localStorage.length; i++){
    tableString = tableString + '<tr><td>'+ localStorage.key(i) + '</td><td>'+ localStorage.getItem(localStorage.key(i))+'</td></tr>)';
  }
  tableString = tableString + '</table>';
  tabla.innerHTML= tableString;
}
